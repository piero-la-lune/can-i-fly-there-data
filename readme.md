# Can I Fly There Data

This is a fork of the data files for the ['Can I Fly There' browser extension](https://gitlab.com/layl/can-i-fly-there) which enhances the experience of the Flight Sim multiplayer game [FSEconomy](https://server.fseconomy.net/).

These files have been updated by community efforts in the FSE forum.

# Aircraft files

There are currently  aircraft files for the 3 editions of Microsoft Flight Simulator (2020) available: Standard, Deluxe and Premium Deluxe. 

# Airport files

The [caniflythere-mfsf.json](https://gitlab.com/jonlowag/can-i-fly-there-data/-/raw/master/caniflythere-msfs.json?inline=false) contains a list of all FSE airports and their mapping to MSFS airports.

There are currently only airports from MSFS included that match the coordinates of FSE airports.

If you notice any incorrect data, please report it [HERE](https://www.fseconomy.net/forum/addon-programs/107410-can-i-fly-there-airport-checking-browser-plugin).

# Contributers

Big thanks to everyone who contributed airport updates in the forums.

* [Layl](https://gitlab.com/layl) is the extension author and has assembled the initial versions of the data files.
* [keithb77](https://fseconomy.net/forum/msfs/104697-match-of-fse-airports-to-msfs?start=0) shared the spreadsheet that was used to create the initial airport database
* [Bahnzo](https://github.com/Bahnzo/canIflythere.json) started the community driven database updates
* [Vladyi74](https://github.com/vladyi74/canIflythere.json) continued maintaining the community updates
* [piero-la-lune](https://github.com/piero-la-lune/canIflythere.json/tree/piero-la-lune-patch-1) implemented an algorithm to generate a (hopefully) complete list of all renamed airports. [(description)](https://github.com/Bahnzo/canIflythere.json/pull/15)